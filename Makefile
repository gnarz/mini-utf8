# simple Makefile, just builds the test.

CFLAGS = -Wall -Wextra -g
#CFLAGS = -Wall -Os

all: check_utf8

test: test_mini_utf8
	./test_mini_utf8

check_utf8: check_utf8.c mini_utf8.h
	$(CC) $(CFLAGS) -o $@ $<

test_mini_utf8: test_mini_utf8.c mini_utf8.h
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f test_mini_utf8 check_utf8

