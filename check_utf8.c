/* check_utf8
 *
 * checks if a file is valid utf8, and outputs line and column of any offending char.
 * Gunnar Zötl <gz@tset.de> 2023
 */

#include <stdio.h>
#include <stdlib.h>
#include "mini_utf8.h"

const char *get_data(FILE *f, int *size)
{
	*size = 0;
	int cap = 1024;
	char *buf = malloc(cap);
	if (buf == NULL) return NULL;
	while (!feof(f)) {
		if (*size == cap) {
			char *oldbuf = buf;
			cap += 2;
			buf = realloc(buf, cap);
			if (buf == 0) {
				free(oldbuf);
				*size = 0;
				return NULL;
			}
		}
		int ch = getc(f);
		buf[*size] = ch == EOF ? 0 : ch;
		*size += 1;
	}
	return buf;
}

int main(int argc, char **argv)
{
	int line = 1, col = 1, size = 0;

	FILE *f = stdin;
	if ((argc == 2 && argv[1][0] == '-') || argc > 2) {
		fprintf(stderr, "usage: %s [-h] [file]\n", argv[0]);
		fprintf(stderr, "  -h    prints this help\n");
		fprintf(stderr, "  reads from stdin or from the file and prints line and (approximated)\n");
		fprintf(stderr, "  column number of any invalid bytes.\n");
		exit(1);
	}

	if (argc == 2) {
		f = fopen(argv[1], "r");
		if (!f) {
			fprintf(stderr, "could not open file %s\n", argv[1]);
		}
	}

	const char *data = get_data(f, &size);
	if (!data) {
		fprintf(stderr, "failed to read data!\n");
		exit(1);
	}

	if (f != stdin) {
		fclose(f);
	}

	const char *ptr = data;
	while (ptr < data + size) {
		int ch =  mini_utf8_decode_f(&ptr, 0);
		if (ch == -1) {
			fprintf(stderr, "invalid utf8 char in line %d, column (appx) %d\n", line, col);
			ptr += 1;
			col += 1;
		} else {
			if (ch == 10) {
				line += 1;
				col += 1;
			}
		}
	}

	exit(0);
}
