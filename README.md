This is a small header-only library to en- and decode utf-8 encoded strings.

This library really only deals with en- and decoding of utf-8 encoded unicode codepoints. However, an attempt has been made to properly treat some of the oddities one might encounter, especially utf-16 relics like utf-8 encoded surrogates. Also, tries to fault invalid unicode codepoints and invalid utf8 sequences.

As the header file is intended to be used standalone, all documentation is contained within mini_utf8.h.
